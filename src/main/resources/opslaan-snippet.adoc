ifndef::imagesdir[:imagesdir: images]
### Je programma op de microbit zetten

#### Je microbit aansluiten

Sluit je microbit eerst aan op je computer. Gebruik hiervoor de kleine usb kabel.
Stop het kleine einde in de microbit, en het grote einde in de computer zoals op de afbeelding.

.Microbit aansluiten
image::opslaan/microbitusb.jpg[Verkenner knop]

#### Programma downloaden
Nu de microbit is aangesloten kunnen we het programma er op zetten. Open je programmeer scherm.

Als eerste moeten we ons programma een naam geven. Vul een goede naam voor je programma in bij het rode vierkant.

.Programma downloaden
image::opslaan/opslaan.png[Programma downloaden]

Druk daarna op de ```Downloaden``` knop.

Het programma bevind zich nu in de ```Download``` map in Windows verkenner.
Deze kunnen we op de volgende manier openen. Druk op de verkenner knop links onderin, zie het volgende plaatje:

.Verkenner knop
image::opslaan/verkennerknop.png[Verkenner knop]

Ga nu links in het menu naar ```Downloads```

.Downloads
image::opslaan/downloads.png[Downloads knop]

Je krijgt nu het scherm te zien met al je downloads. Om jou programma makkelijk te vinden gaan we sorteren op ```Gewijzigd op```
Dit kun je doen door op het kopje ```Gewijzigd op``` te drukken bij het rode pijltje. Het nieuwste bestand zou nu bovenaan moeten staan. Als dit niet het geval is moet je nog een keer op de knop drukken.

.Sorteren
image::opslaan/programmabestanden.png[Downloads knop]

Als het goed is staat jou programma nu bovenaan. Sleep het programma links-midden naar de map ```Microbit```.

Je krijgt even een laadbalk te zien, en als deze afgelopen is staat je programma op de microbit!